﻿using NBench;
using System;

namespace PM.PerformanceTest
{
    class Program
    {
        static int Main(string[] args)
        {
            return NBenchRunner.Run<Program>();
        }
    }
}

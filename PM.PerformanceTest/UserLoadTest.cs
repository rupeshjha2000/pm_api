﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NBench;
using NUnit.Framework;
using PM.UserService.Model;
using PM.UserService.Repository;
using PMService.Controllers;
using PMService.Model;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace PM.PerformanceTest
{
   public class UserLoadTest
    {
        private Counter _counter;

        [PerfSetup]
        public void Setup(BenchmarkContext context)
        {
            _counter = context.GetCounter("TestCounter");
        }

        readonly UserController _userController;
        readonly Mock<IUserRepository> mockUserRepo = new Mock<IUserRepository>();

        public UserLoadTest()
        {
            this._userController = new UserController(mockUserRepo.Object);
            this._userController = new UserController(mockUserRepo.Object)
            {
                ControllerContext = new ControllerContext()
                { HttpContext = new DefaultHttpContext() }
            };

        }
        [PerfBenchmark(Description = "Test to gauge the impact of having multiple things to measure on a benchmark.",
           NumberOfIterations = 500, RunMode = RunMode.Iterations, RunTimeMilliseconds = 600000, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.GreaterThan, 1000.0d)]
        [Test]
        public void GetUsers()
        {
            var users = new List<User>() {
                   new User
                    {
                        empId = "436125",
                        firstName = "Rupesh",
                        id = 1,
                        lastName = "Jha"
                    }
                };

            mockUserRepo.Setup(c => c.GetAllUsers()).Returns(users);
            var response = (OkObjectResult)_userController.Get();
            response.ShouldNotBeNull();
            var userlist = (List<User>)response.Value;
            userlist.Count.ShouldBeGreaterThan(0);
            userlist.Count.ShouldBeGreaterThanOrEqualTo(users.Count);
            userlist[0].firstName.ShouldBe(users[0].firstName);

            _counter.Increment();
        }

        [PerfBenchmark(Description = "Test to gauge the impact of having multiple things to measure on a benchmark.",
           NumberOfIterations = 500, RunMode = RunMode.Iterations, RunTimeMilliseconds = 600000, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.GreaterThan, 1000.0d)]
        [Test]
        public void AddUser()
        {
            var user = new User
            {
                firstName = "Rupesh Kumar",
                lastName = "Unit Test1",
                empId = "999999",
                id = 999
            };
            UserViewModel returnData = new UserViewModel() { Result = true, Message = "success", user = user };


            var request = new User
            {
                firstName = "Rupesh Kumar",
                lastName = "Unit Test1",
                empId = "999999",
                id = 999
            };

            //mockUserRepo.Setup(c => c.Add(It.IsAny<User>())).Returns(returnData);

            mockUserRepo.Setup(service => service.Add(user));


            OkObjectResult uvResult = (OkObjectResult)_userController.Post(user);

            Assert.AreEqual(200, uvResult.StatusCode);

            uvResult.ShouldNotBeNull();
            uvResult.StatusCode.Value.ShouldBe(200);

            _counter.Increment();

        }

        [PerfBenchmark(Description = "Test to gauge the impact of having multiple things to measure on a benchmark.",
          NumberOfIterations = 500, RunMode = RunMode.Iterations, RunTimeMilliseconds = 600000, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.GreaterThan, 1000.0d)]
        [Test]
        public void ModifyUser()
        {

            var user = new User
            {
                id = 1,
                empId = "436125",
                firstName = "Rupesh",
                lastName = "jha"
            };
            UserViewModel returnData = new UserViewModel() { Result = true, Message = "success", user = user };


            var request = new User
            {
                id = 1,
                empId = "436125",
                firstName = "Rupesh",
                lastName = "jha"
            };
            int id = 1;

            mockUserRepo.Setup(c => c.Update(It.IsAny<User>())).Returns(returnData);
            var response = (JsonResult)_userController.Put(id, request);
            response.ShouldNotBeNull();
            UserViewModel uvResult = (UserViewModel)response.Value;
            uvResult.ShouldBe(returnData);
            uvResult.Message.ShouldBe(returnData.Message);
            _counter.Increment();
        }

        [PerfBenchmark(Description = "Test to gauge the impact of having multiple things to measure on a benchmark.",
          NumberOfIterations = 500, RunMode = RunMode.Iterations, RunTimeMilliseconds = 600000, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.GreaterThan, 1000.0d)]
        [Test]
        public void GetUser()
        {
            var user = new User
            {
                id = 1,
                empId = "436125",
                firstName = "Rupesh",
                lastName = "Jha"
            };
            mockUserRepo.Setup(service => service.GetUser(It.IsAny<int>())).Returns(user);

            var response = (OkObjectResult)_userController.Get(1);
            response.ShouldNotBeNull();
            response.StatusCode.Value.ShouldBe(200);

            _counter.Increment();
        }
    }
}

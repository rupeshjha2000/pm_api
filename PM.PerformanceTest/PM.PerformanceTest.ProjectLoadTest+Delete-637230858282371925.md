﻿# PM.PerformanceTest.ProjectLoadTest+Delete
__Test to gauge the impact of having multiple things to measure on a benchmark.__
_21-04-2020 17:10:28_
### System Info
```ini
NBench=NBench, Version=2.0.1.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Iterations, TestMode=Test
NumberOfIterations=500, MaximumRunTime=00:10:00
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|[Counter] TestCounter |      operations |            1.00 |            1.00 |            1.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|[Counter] TestCounter |      operations |        2,556.24 |        2,165.12 |          124.57 |          242.60 |

### Raw Data
#### [Counter] TestCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |            1.00 |        2,078.14 |     4,81,200.00 |
|               2 |            1.00 |        2,118.64 |     4,72,000.00 |
|               3 |            1.00 |        2,111.04 |     4,73,700.00 |
|               4 |            1.00 |        2,201.67 |     4,54,200.00 |
|               5 |            1.00 |        2,027.99 |     4,93,100.00 |
|               6 |            1.00 |        2,106.15 |     4,74,800.00 |
|               7 |            1.00 |        2,194.91 |     4,55,600.00 |
|               8 |            1.00 |        2,182.93 |     4,58,100.00 |
|               9 |            1.00 |        2,182.45 |     4,58,200.00 |
|              10 |            1.00 |        2,137.21 |     4,67,900.00 |
|              11 |            1.00 |        1,971.61 |     5,07,200.00 |
|              12 |            1.00 |        2,060.58 |     4,85,300.00 |
|              13 |            1.00 |        1,993.22 |     5,01,700.00 |
|              14 |            1.00 |        1,981.38 |     5,04,700.00 |
|              15 |            1.00 |        2,127.21 |     4,70,100.00 |
|              16 |            1.00 |        1,935.36 |     5,16,700.00 |
|              17 |            1.00 |        2,111.49 |     4,73,600.00 |
|              18 |            1.00 |        2,185.31 |     4,57,600.00 |
|              19 |            1.00 |        1,868.46 |     5,35,200.00 |
|              20 |            1.00 |        2,178.17 |     4,59,100.00 |
|              21 |            1.00 |        1,972.39 |     5,07,000.00 |
|              22 |            1.00 |        2,180.55 |     4,58,600.00 |
|              23 |            1.00 |        2,124.04 |     4,70,800.00 |
|              24 |            1.00 |        2,183.41 |     4,58,000.00 |
|              25 |            1.00 |        2,285.19 |     4,37,600.00 |
|              26 |            1.00 |        2,307.87 |     4,33,300.00 |
|              27 |            1.00 |        2,053.39 |     4,87,000.00 |
|              28 |            1.00 |        2,222.72 |     4,49,900.00 |
|              29 |            1.00 |        2,060.16 |     4,85,400.00 |
|              30 |            1.00 |        2,282.58 |     4,38,100.00 |
|              31 |            1.00 |        2,224.69 |     4,49,500.00 |
|              32 |            1.00 |        2,222.72 |     4,49,900.00 |
|              33 |            1.00 |        2,535.50 |     3,94,400.00 |
|              34 |            1.00 |        2,545.18 |     3,92,900.00 |
|              35 |            1.00 |        2,367.98 |     4,22,300.00 |
|              36 |            1.00 |        2,556.24 |     3,91,200.00 |
|              37 |            1.00 |        2,299.38 |     4,34,900.00 |
|              38 |            1.00 |        2,541.30 |     3,93,500.00 |
|              39 |            1.00 |        2,474.02 |     4,04,200.00 |
|              40 |            1.00 |        2,529.72 |     3,95,300.00 |
|              41 |            1.00 |        2,479.54 |     4,03,300.00 |
|              42 |            1.00 |        2,552.32 |     3,91,800.00 |
|              43 |            1.00 |        2,331.00 |     4,29,000.00 |
|              44 |            1.00 |        2,540.65 |     3,93,600.00 |
|              45 |            1.00 |        2,467.92 |     4,05,200.00 |
|              46 |            1.00 |        2,370.79 |     4,21,800.00 |
|              47 |            1.00 |        2,470.36 |     4,04,800.00 |
|              48 |            1.00 |        2,496.26 |     4,00,600.00 |
|              49 |            1.00 |        2,452.18 |     4,07,800.00 |
|              50 |            1.00 |        2,293.58 |     4,36,000.00 |
|              51 |            1.00 |        2,131.74 |     4,69,100.00 |
|              52 |            1.00 |        2,227.67 |     4,48,900.00 |
|              53 |            1.00 |        2,190.58 |     4,56,500.00 |
|              54 |            1.00 |        2,244.17 |     4,45,600.00 |
|              55 |            1.00 |        2,306.81 |     4,33,500.00 |
|              56 |            1.00 |        2,116.40 |     4,72,500.00 |
|              57 |            1.00 |        2,237.14 |     4,47,000.00 |
|              58 |            1.00 |        2,240.14 |     4,46,400.00 |
|              59 |            1.00 |        2,148.23 |     4,65,500.00 |
|              60 |            1.00 |        2,304.68 |     4,33,900.00 |
|              61 |            1.00 |        2,219.76 |     4,50,500.00 |
|              62 |            1.00 |        2,179.60 |     4,58,800.00 |
|              63 |            1.00 |        2,296.74 |     4,35,400.00 |
|              64 |            1.00 |        2,293.58 |     4,36,000.00 |
|              65 |            1.00 |        2,248.71 |     4,44,700.00 |
|              66 |            1.00 |        2,228.16 |     4,48,800.00 |
|              67 |            1.00 |        2,192.98 |     4,56,000.00 |
|              68 |            1.00 |        2,231.15 |     4,48,200.00 |
|              69 |            1.00 |        2,200.70 |     4,54,400.00 |
|              70 |            1.00 |        2,297.27 |     4,35,300.00 |
|              71 |            1.00 |        2,217.29 |     4,51,000.00 |
|              72 |            1.00 |        2,229.16 |     4,48,600.00 |
|              73 |            1.00 |        2,229.65 |     4,48,500.00 |
|              74 |            1.00 |        2,223.21 |     4,49,800.00 |
|              75 |            1.00 |        2,143.16 |     4,66,600.00 |
|              76 |            1.00 |        2,225.19 |     4,49,400.00 |
|              77 |            1.00 |        2,183.41 |     4,58,000.00 |
|              78 |            1.00 |        2,245.17 |     4,45,400.00 |
|              79 |            1.00 |        2,217.29 |     4,51,000.00 |
|              80 |            1.00 |        2,299.91 |     4,34,800.00 |
|              81 |            1.00 |        2,237.64 |     4,46,900.00 |
|              82 |            1.00 |        2,290.95 |     4,36,500.00 |
|              83 |            1.00 |        2,166.85 |     4,61,500.00 |
|              84 |            1.00 |        2,196.84 |     4,55,200.00 |
|              85 |            1.00 |        2,214.84 |     4,51,500.00 |
|              86 |            1.00 |        2,226.68 |     4,49,100.00 |
|              87 |            1.00 |        2,226.68 |     4,49,100.00 |
|              88 |            1.00 |        2,275.31 |     4,39,500.00 |
|              89 |            1.00 |        2,222.72 |     4,49,900.00 |
|              90 |            1.00 |        2,228.66 |     4,48,700.00 |
|              91 |            1.00 |        2,143.16 |     4,66,600.00 |
|              92 |            1.00 |        2,213.86 |     4,51,700.00 |
|              93 |            1.00 |        2,198.77 |     4,54,800.00 |
|              94 |            1.00 |        2,246.18 |     4,45,200.00 |
|              95 |            1.00 |        2,232.14 |     4,48,000.00 |
|              96 |            1.00 |        2,229.16 |     4,48,600.00 |
|              97 |            1.00 |        2,227.67 |     4,48,900.00 |
|              98 |            1.00 |        2,208.48 |     4,52,800.00 |
|              99 |            1.00 |        2,075.98 |     4,81,700.00 |
|             100 |            1.00 |        2,288.85 |     4,36,900.00 |
|             101 |            1.00 |        2,201.67 |     4,54,200.00 |
|             102 |            1.00 |        2,219.76 |     4,50,500.00 |
|             103 |            1.00 |        1,275.51 |     7,84,000.00 |
|             104 |            1.00 |        1,719.10 |     5,81,700.00 |
|             105 |            1.00 |        1,612.38 |     6,20,200.00 |
|             106 |            1.00 |        1,641.23 |     6,09,300.00 |
|             107 |            1.00 |        2,125.85 |     4,70,400.00 |
|             108 |            1.00 |        1,589.32 |     6,29,200.00 |
|             109 |            1.00 |        2,224.69 |     4,49,500.00 |
|             110 |            1.00 |        2,171.55 |     4,60,500.00 |
|             111 |            1.00 |        2,176.28 |     4,59,500.00 |
|             112 |            1.00 |        2,223.21 |     4,49,800.00 |
|             113 |            1.00 |        2,181.98 |     4,58,300.00 |
|             114 |            1.00 |        2,225.68 |     4,49,300.00 |
|             115 |            1.00 |        2,234.14 |     4,47,600.00 |
|             116 |            1.00 |        2,202.64 |     4,54,000.00 |
|             117 |            1.00 |        2,230.65 |     4,48,300.00 |
|             118 |            1.00 |        2,214.35 |     4,51,600.00 |
|             119 |            1.00 |        2,270.15 |     4,40,500.00 |
|             120 |            1.00 |        2,227.17 |     4,49,000.00 |
|             121 |            1.00 |        2,149.61 |     4,65,200.00 |
|             122 |            1.00 |        2,236.64 |     4,47,100.00 |
|             123 |            1.00 |        2,231.15 |     4,48,200.00 |
|             124 |            1.00 |        2,202.64 |     4,54,000.00 |
|             125 |            1.00 |        2,220.25 |     4,50,400.00 |
|             126 |            1.00 |        1,699.81 |     5,88,300.00 |
|             127 |            1.00 |        1,433.28 |     6,97,700.00 |
|             128 |            1.00 |        1,443.42 |     6,92,800.00 |
|             129 |            1.00 |        2,095.12 |     4,77,300.00 |
|             130 |            1.00 |        2,111.04 |     4,73,700.00 |
|             131 |            1.00 |        2,295.16 |     4,35,700.00 |
|             132 |            1.00 |        2,292.53 |     4,36,200.00 |
|             133 |            1.00 |        2,222.72 |     4,49,900.00 |
|             134 |            1.00 |        2,313.74 |     4,32,200.00 |
|             135 |            1.00 |        2,079.43 |     4,80,900.00 |
|             136 |            1.00 |        2,281.02 |     4,38,400.00 |
|             137 |            1.00 |        2,311.60 |     4,32,600.00 |
|             138 |            1.00 |        1,921.97 |     5,20,300.00 |
|             139 |            1.00 |        2,088.55 |     4,78,800.00 |
|             140 |            1.00 |        2,238.14 |     4,46,800.00 |
|             141 |            1.00 |        1,714.38 |     5,83,300.00 |
|             142 |            1.00 |        1,319.78 |     7,57,700.00 |
|             143 |            1.00 |        2,199.25 |     4,54,700.00 |
|             144 |            1.00 |        2,061.86 |     4,85,000.00 |
|             145 |            1.00 |        2,195.39 |     4,55,500.00 |
|             146 |            1.00 |        2,081.60 |     4,80,400.00 |
|             147 |            1.00 |        2,230.65 |     4,48,300.00 |
|             148 |            1.00 |        1,822.82 |     5,48,600.00 |
|             149 |            1.00 |        2,053.39 |     4,87,000.00 |
|             150 |            1.00 |        2,226.68 |     4,49,100.00 |
|             151 |            1.00 |        1,901.86 |     5,25,800.00 |
|             152 |            1.00 |        2,204.10 |     4,53,700.00 |
|             153 |            1.00 |        2,233.64 |     4,47,700.00 |
|             154 |            1.00 |        1,814.22 |     5,51,200.00 |
|             155 |            1.00 |        2,198.77 |     4,54,800.00 |
|             156 |            1.00 |        2,135.38 |     4,68,300.00 |
|             157 |            1.00 |        2,302.56 |     4,34,300.00 |
|             158 |            1.00 |        2,236.64 |     4,47,100.00 |
|             159 |            1.00 |        2,217.79 |     4,50,900.00 |
|             160 |            1.00 |        2,227.67 |     4,48,900.00 |
|             161 |            1.00 |        2,224.69 |     4,49,500.00 |
|             162 |            1.00 |        2,242.15 |     4,46,000.00 |
|             163 |            1.00 |        2,311.60 |     4,32,600.00 |
|             164 |            1.00 |        2,076.41 |     4,81,600.00 |
|             165 |            1.00 |        2,194.91 |     4,55,600.00 |
|             166 |            1.00 |        1,868.11 |     5,35,300.00 |
|             167 |            1.00 |        2,151.46 |     4,64,800.00 |
|             168 |            1.00 |        2,242.66 |     4,45,900.00 |
|             169 |            1.00 |        2,285.19 |     4,37,600.00 |
|             170 |            1.00 |        2,290.95 |     4,36,500.00 |
|             171 |            1.00 |        2,144.54 |     4,66,300.00 |
|             172 |            1.00 |        2,279.46 |     4,38,700.00 |
|             173 |            1.00 |        2,303.09 |     4,34,200.00 |
|             174 |            1.00 |        2,002.80 |     4,99,300.00 |
|             175 |            1.00 |        2,292.00 |     4,36,300.00 |
|             176 |            1.00 |          124.57 |    80,27,300.00 |
|             177 |            1.00 |        2,149.61 |     4,65,200.00 |
|             178 |            1.00 |        2,096.44 |     4,77,000.00 |
|             179 |            1.00 |        2,232.14 |     4,48,000.00 |
|             180 |            1.00 |        2,294.10 |     4,35,900.00 |
|             181 |            1.00 |        2,211.90 |     4,52,100.00 |
|             182 |            1.00 |        2,299.91 |     4,34,800.00 |
|             183 |            1.00 |        2,216.31 |     4,51,200.00 |
|             184 |            1.00 |        2,219.26 |     4,50,600.00 |
|             185 |            1.00 |        2,306.27 |     4,33,600.00 |
|             186 |            1.00 |        2,225.19 |     4,49,400.00 |
|             187 |            1.00 |        2,252.25 |     4,44,000.00 |
|             188 |            1.00 |        2,190.10 |     4,56,600.00 |
|             189 |            1.00 |        2,097.32 |     4,76,800.00 |
|             190 |            1.00 |        2,294.63 |     4,35,800.00 |
|             191 |            1.00 |        2,274.80 |     4,39,600.00 |
|             192 |            1.00 |        2,029.63 |     4,92,700.00 |
|             193 |            1.00 |        2,232.64 |     4,47,900.00 |
|             194 |            1.00 |        2,248.20 |     4,44,800.00 |
|             195 |            1.00 |        2,290.43 |     4,36,600.00 |
|             196 |            1.00 |        2,209.46 |     4,52,600.00 |
|             197 |            1.00 |        2,199.25 |     4,54,700.00 |
|             198 |            1.00 |        2,229.16 |     4,48,600.00 |
|             199 |            1.00 |        2,230.65 |     4,48,300.00 |
|             200 |            1.00 |        2,220.25 |     4,50,400.00 |
|             201 |            1.00 |        2,233.14 |     4,47,800.00 |
|             202 |            1.00 |        2,037.91 |     4,90,700.00 |
|             203 |            1.00 |        2,303.62 |     4,34,100.00 |
|             204 |            1.00 |        2,131.29 |     4,69,200.00 |
|             205 |            1.00 |        2,241.15 |     4,46,200.00 |
|             206 |            1.00 |        2,225.19 |     4,49,400.00 |
|             207 |            1.00 |        2,300.97 |     4,34,600.00 |
|             208 |            1.00 |        2,303.62 |     4,34,100.00 |
|             209 |            1.00 |        2,176.28 |     4,59,500.00 |
|             210 |            1.00 |        2,274.28 |     4,39,700.00 |
|             211 |            1.00 |        2,113.27 |     4,73,200.00 |
|             212 |            1.00 |        2,293.58 |     4,36,000.00 |
|             213 |            1.00 |        2,189.62 |     4,56,700.00 |
|             214 |            1.00 |        2,285.19 |     4,37,600.00 |
|             215 |            1.00 |        2,246.69 |     4,45,100.00 |
|             216 |            1.00 |        2,270.15 |     4,40,500.00 |
|             217 |            1.00 |        2,223.70 |     4,49,700.00 |
|             218 |            1.00 |        2,233.64 |     4,47,700.00 |
|             219 |            1.00 |        2,165.91 |     4,61,700.00 |
|             220 |            1.00 |        2,306.27 |     4,33,600.00 |
|             221 |            1.00 |        2,211.41 |     4,52,200.00 |
|             222 |            1.00 |        2,301.50 |     4,34,500.00 |
|             223 |            1.00 |        2,221.24 |     4,50,200.00 |
|             224 |            1.00 |        2,219.76 |     4,50,500.00 |
|             225 |            1.00 |        2,210.43 |     4,52,400.00 |
|             226 |            1.00 |        2,189.14 |     4,56,800.00 |
|             227 |            1.00 |        2,292.00 |     4,36,300.00 |
|             228 |            1.00 |        2,313.21 |     4,32,300.00 |
|             229 |            1.00 |        2,273.76 |     4,39,800.00 |
|             230 |            1.00 |        2,312.14 |     4,32,500.00 |
|             231 |            1.00 |        2,234.14 |     4,47,600.00 |
|             232 |            1.00 |        2,083.77 |     4,79,900.00 |
|             233 |            1.00 |        2,139.50 |     4,67,400.00 |
|             234 |            1.00 |        2,210.43 |     4,52,400.00 |
|             235 |            1.00 |        2,306.81 |     4,33,500.00 |
|             236 |            1.00 |        2,159.36 |     4,63,100.00 |
|             237 |            1.00 |        2,240.14 |     4,46,400.00 |
|             238 |            1.00 |        2,218.77 |     4,50,700.00 |
|             239 |            1.00 |        2,242.66 |     4,45,900.00 |
|             240 |            1.00 |        2,189.14 |     4,56,800.00 |
|             241 |            1.00 |        2,230.15 |     4,48,400.00 |
|             242 |            1.00 |        2,107.04 |     4,74,600.00 |
|             243 |            1.00 |        2,181.98 |     4,58,300.00 |
|             244 |            1.00 |        2,293.58 |     4,36,000.00 |
|             245 |            1.00 |        2,302.56 |     4,34,300.00 |
|             246 |            1.00 |        2,207.02 |     4,53,100.00 |
|             247 |            1.00 |        2,299.38 |     4,34,900.00 |
|             248 |            1.00 |        2,187.23 |     4,57,200.00 |
|             249 |            1.00 |        2,231.64 |     4,48,100.00 |
|             250 |            1.00 |        2,238.64 |     4,46,700.00 |
|             251 |            1.00 |        2,297.27 |     4,35,300.00 |
|             252 |            1.00 |        2,232.64 |     4,47,900.00 |
|             253 |            1.00 |        2,295.68 |     4,35,600.00 |
|             254 |            1.00 |        2,144.08 |     4,66,400.00 |
|             255 |            1.00 |        2,306.81 |     4,33,500.00 |
|             256 |            1.00 |        2,208.97 |     4,52,700.00 |
|             257 |            1.00 |        2,310.54 |     4,32,800.00 |
|             258 |            1.00 |        2,219.26 |     4,50,600.00 |
|             259 |            1.00 |        2,226.18 |     4,49,200.00 |
|             260 |            1.00 |        2,234.14 |     4,47,600.00 |
|             261 |            1.00 |        2,092.93 |     4,77,800.00 |
|             262 |            1.00 |        2,303.09 |     4,34,200.00 |
|             263 |            1.00 |        1,322.75 |     7,56,000.00 |
|             264 |            1.00 |        1,645.28 |     6,07,800.00 |
|             265 |            1.00 |        2,213.86 |     4,51,700.00 |
|             266 |            1.00 |        2,197.32 |     4,55,100.00 |
|             267 |            1.00 |        1,359.80 |     7,35,400.00 |
|             268 |            1.00 |        1,323.80 |     7,55,400.00 |
|             269 |            1.00 |        1,171.51 |     8,53,600.00 |
|             270 |            1.00 |        2,191.54 |     4,56,300.00 |
|             271 |            1.00 |        2,265.01 |     4,41,500.00 |
|             272 |            1.00 |        2,084.20 |     4,79,800.00 |
|             273 |            1.00 |        2,123.59 |     4,70,900.00 |
|             274 |            1.00 |        2,289.38 |     4,36,800.00 |
|             275 |            1.00 |        2,058.04 |     4,85,900.00 |
|             276 |            1.00 |        2,225.19 |     4,49,400.00 |
|             277 |            1.00 |        2,156.10 |     4,63,800.00 |
|             278 |            1.00 |        2,297.27 |     4,35,300.00 |
|             279 |            1.00 |        1,587.30 |     6,30,000.00 |
|             280 |            1.00 |        1,234.87 |     8,09,800.00 |
|             281 |            1.00 |        1,520.45 |     6,57,700.00 |
|             282 |            1.00 |        1,332.80 |     7,50,300.00 |
|             283 |            1.00 |        1,442.38 |     6,93,300.00 |
|             284 |            1.00 |        1,286.17 |     7,77,500.00 |
|             285 |            1.00 |        2,209.46 |     4,52,600.00 |
|             286 |            1.00 |        2,064.41 |     4,84,400.00 |
|             287 |            1.00 |        2,265.52 |     4,41,400.00 |
|             288 |            1.00 |        2,279.46 |     4,38,700.00 |
|             289 |            1.00 |        2,201.19 |     4,54,300.00 |
|             290 |            1.00 |        1,678.98 |     5,95,600.00 |
|             291 |            1.00 |        1,578.78 |     6,33,400.00 |
|             292 |            1.00 |        1,200.91 |     8,32,700.00 |
|             293 |            1.00 |        2,212.39 |     4,52,000.00 |
|             294 |            1.00 |        2,282.06 |     4,38,200.00 |
|             295 |            1.00 |        2,126.75 |     4,70,200.00 |
|             296 |            1.00 |        2,300.44 |     4,34,700.00 |
|             297 |            1.00 |        2,216.31 |     4,51,200.00 |
|             298 |            1.00 |        2,300.44 |     4,34,700.00 |
|             299 |            1.00 |        2,233.14 |     4,47,800.00 |
|             300 |            1.00 |        2,051.28 |     4,87,500.00 |
|             301 |            1.00 |        2,164.03 |     4,62,100.00 |
|             302 |            1.00 |        2,300.97 |     4,34,600.00 |
|             303 |            1.00 |        1,640.15 |     6,09,700.00 |
|             304 |            1.00 |        1,243.94 |     8,03,900.00 |
|             305 |            1.00 |        2,151.46 |     4,64,800.00 |
|             306 |            1.00 |        2,189.62 |     4,56,700.00 |
|             307 |            1.00 |        2,284.15 |     4,37,800.00 |
|             308 |            1.00 |        2,290.95 |     4,36,500.00 |
|             309 |            1.00 |        2,150.08 |     4,65,100.00 |
|             310 |            1.00 |        2,279.98 |     4,38,600.00 |
|             311 |            1.00 |        1,967.73 |     5,08,200.00 |
|             312 |            1.00 |        2,164.50 |     4,62,000.00 |
|             313 |            1.00 |        2,215.33 |     4,51,400.00 |
|             314 |            1.00 |        2,217.29 |     4,51,000.00 |
|             315 |            1.00 |        2,289.90 |     4,36,700.00 |
|             316 |            1.00 |        1,670.01 |     5,98,800.00 |
|             317 |            1.00 |        1,313.54 |     7,61,300.00 |
|             318 |            1.00 |        1,684.35 |     5,93,700.00 |
|             319 |            1.00 |        1,724.73 |     5,79,800.00 |
|             320 |            1.00 |        2,221.73 |     4,50,100.00 |
|             321 |            1.00 |        2,290.43 |     4,36,600.00 |
|             322 |            1.00 |        2,142.70 |     4,66,700.00 |
|             323 |            1.00 |        2,222.72 |     4,49,900.00 |
|             324 |            1.00 |        2,245.68 |     4,45,300.00 |
|             325 |            1.00 |        2,298.85 |     4,35,000.00 |
|             326 |            1.00 |        2,226.18 |     4,49,200.00 |
|             327 |            1.00 |        2,198.29 |     4,54,900.00 |
|             328 |            1.00 |        2,302.03 |     4,34,400.00 |
|             329 |            1.00 |        2,304.68 |     4,33,900.00 |
|             330 |            1.00 |        1,655.08 |     6,04,200.00 |
|             331 |            1.00 |        1,576.79 |     6,34,200.00 |
|             332 |            1.00 |        2,220.74 |     4,50,300.00 |
|             333 |            1.00 |        1,782.85 |     5,60,900.00 |
|             334 |            1.00 |        2,224.69 |     4,49,500.00 |
|             335 |            1.00 |        2,302.03 |     4,34,400.00 |
|             336 |            1.00 |          998.70 |    10,01,300.00 |
|             337 |            1.00 |        1,701.84 |     5,87,600.00 |
|             338 |            1.00 |        1,763.67 |     5,67,000.00 |
|             339 |            1.00 |        2,203.61 |     4,53,800.00 |
|             340 |            1.00 |        2,305.74 |     4,33,700.00 |
|             341 |            1.00 |        1,640.96 |     6,09,400.00 |
|             342 |            1.00 |        1,647.72 |     6,06,900.00 |
|             343 |            1.00 |        2,229.65 |     4,48,500.00 |
|             344 |            1.00 |        2,179.60 |     4,58,800.00 |
|             345 |            1.00 |        2,262.44 |     4,42,000.00 |
|             346 |            1.00 |        2,295.68 |     4,35,600.00 |
|             347 |            1.00 |        2,179.12 |     4,58,900.00 |
|             348 |            1.00 |        2,235.14 |     4,47,400.00 |
|             349 |            1.00 |        2,207.51 |     4,53,000.00 |
|             350 |            1.00 |        2,232.14 |     4,48,000.00 |
|             351 |            1.00 |        2,212.39 |     4,52,000.00 |
|             352 |            1.00 |        2,258.36 |     4,42,800.00 |
|             353 |            1.00 |        2,238.14 |     4,46,800.00 |
|             354 |            1.00 |        2,164.50 |     4,62,000.00 |
|             355 |            1.00 |        2,236.64 |     4,47,100.00 |
|             356 |            1.00 |        2,005.62 |     4,98,600.00 |
|             357 |            1.00 |        2,243.16 |     4,45,800.00 |
|             358 |            1.00 |        2,239.14 |     4,46,600.00 |
|             359 |            1.00 |        2,262.44 |     4,42,000.00 |
|             360 |            1.00 |        2,301.50 |     4,34,500.00 |
|             361 |            1.00 |        2,294.10 |     4,35,900.00 |
|             362 |            1.00 |        2,266.03 |     4,41,300.00 |
|             363 |            1.00 |        2,217.79 |     4,50,900.00 |
|             364 |            1.00 |        2,285.71 |     4,37,500.00 |
|             365 |            1.00 |        2,229.65 |     4,48,500.00 |
|             366 |            1.00 |        2,310.00 |     4,32,900.00 |
|             367 |            1.00 |        2,205.56 |     4,53,400.00 |
|             368 |            1.00 |        2,249.21 |     4,44,600.00 |
|             369 |            1.00 |        2,308.40 |     4,33,200.00 |
|             370 |            1.00 |        2,247.19 |     4,45,000.00 |
|             371 |            1.00 |        2,292.00 |     4,36,300.00 |
|             372 |            1.00 |        2,262.44 |     4,42,000.00 |
|             373 |            1.00 |        2,309.47 |     4,33,000.00 |
|             374 |            1.00 |        2,246.18 |     4,45,200.00 |
|             375 |            1.00 |        2,209.94 |     4,52,500.00 |
|             376 |            1.00 |        2,244.17 |     4,45,600.00 |
|             377 |            1.00 |        2,105.26 |     4,75,000.00 |
|             378 |            1.00 |        2,286.24 |     4,37,400.00 |
|             379 |            1.00 |        2,298.85 |     4,35,000.00 |
|             380 |            1.00 |        2,273.76 |     4,39,800.00 |
|             381 |            1.00 |        2,221.73 |     4,50,100.00 |
|             382 |            1.00 |        2,154.71 |     4,64,100.00 |
|             383 |            1.00 |        2,272.73 |     4,40,000.00 |
|             384 |            1.00 |        2,231.64 |     4,48,100.00 |
|             385 |            1.00 |        2,255.30 |     4,43,400.00 |
|             386 |            1.00 |        2,039.57 |     4,90,300.00 |
|             387 |            1.00 |        2,287.81 |     4,37,100.00 |
|             388 |            1.00 |        2,290.43 |     4,36,600.00 |
|             389 |            1.00 |        2,228.16 |     4,48,800.00 |
|             390 |            1.00 |        2,277.90 |     4,39,000.00 |
|             391 |            1.00 |        2,288.85 |     4,36,900.00 |
|             392 |            1.00 |        2,197.32 |     4,55,100.00 |
|             393 |            1.00 |        2,307.34 |     4,33,400.00 |
|             394 |            1.00 |        2,236.14 |     4,47,200.00 |
|             395 |            1.00 |        2,231.64 |     4,48,100.00 |
|             396 |            1.00 |        2,298.85 |     4,35,000.00 |
|             397 |            1.00 |        2,303.62 |     4,34,100.00 |
|             398 |            1.00 |        2,262.44 |     4,42,000.00 |
|             399 |            1.00 |        2,221.24 |     4,50,200.00 |
|             400 |            1.00 |        2,238.64 |     4,46,700.00 |
|             401 |            1.00 |        2,052.97 |     4,87,100.00 |
|             402 |            1.00 |        2,311.07 |     4,32,700.00 |
|             403 |            1.00 |        2,297.79 |     4,35,200.00 |
|             404 |            1.00 |        2,292.53 |     4,36,200.00 |
|             405 |            1.00 |        2,305.74 |     4,33,700.00 |
|             406 |            1.00 |        2,188.18 |     4,57,000.00 |
|             407 |            1.00 |        2,301.50 |     4,34,500.00 |
|             408 |            1.00 |        2,226.68 |     4,49,100.00 |
|             409 |            1.00 |        2,302.03 |     4,34,400.00 |
|             410 |            1.00 |        2,306.81 |     4,33,500.00 |
|             411 |            1.00 |        2,203.61 |     4,53,800.00 |
|             412 |            1.00 |        2,303.62 |     4,34,100.00 |
|             413 |            1.00 |        2,292.53 |     4,36,200.00 |
|             414 |            1.00 |        2,193.94 |     4,55,800.00 |
|             415 |            1.00 |        2,312.67 |     4,32,400.00 |
|             416 |            1.00 |        2,251.24 |     4,44,200.00 |
|             417 |            1.00 |        2,297.79 |     4,35,200.00 |
|             418 |            1.00 |        2,225.19 |     4,49,400.00 |
|             419 |            1.00 |        2,216.80 |     4,51,100.00 |
|             420 |            1.00 |        2,314.81 |     4,32,000.00 |
|             421 |            1.00 |        2,296.74 |     4,35,400.00 |
|             422 |            1.00 |        2,216.80 |     4,51,100.00 |
|             423 |            1.00 |        2,238.14 |     4,46,800.00 |
|             424 |            1.00 |        2,253.27 |     4,43,800.00 |
|             425 |            1.00 |        2,306.81 |     4,33,500.00 |
|             426 |            1.00 |        1,905.12 |     5,24,900.00 |
|             427 |            1.00 |        2,276.87 |     4,39,200.00 |
|             428 |            1.00 |        2,306.81 |     4,33,500.00 |
|             429 |            1.00 |        2,247.70 |     4,44,900.00 |
|             430 |            1.00 |        2,297.27 |     4,35,300.00 |
|             431 |            1.00 |        2,096.44 |     4,77,000.00 |
|             432 |            1.00 |        2,214.84 |     4,51,500.00 |
|             433 |            1.00 |        1,138.95 |     8,78,000.00 |
|             434 |            1.00 |        2,281.02 |     4,38,400.00 |
|             435 |            1.00 |        2,301.50 |     4,34,500.00 |
|             436 |            1.00 |        2,286.76 |     4,37,300.00 |
|             437 |            1.00 |        2,231.15 |     4,48,200.00 |
|             438 |            1.00 |        2,297.79 |     4,35,200.00 |
|             439 |            1.00 |        2,224.69 |     4,49,500.00 |
|             440 |            1.00 |        2,235.14 |     4,47,400.00 |
|             441 |            1.00 |        2,263.47 |     4,41,800.00 |
|             442 |            1.00 |        2,176.75 |     4,59,400.00 |
|             443 |            1.00 |        2,307.34 |     4,33,400.00 |
|             444 |            1.00 |        2,167.32 |     4,61,400.00 |
|             445 |            1.00 |        2,223.70 |     4,49,700.00 |
|             446 |            1.00 |        2,294.10 |     4,35,900.00 |
|             447 |            1.00 |        2,299.91 |     4,34,800.00 |
|             448 |            1.00 |        2,207.99 |     4,52,900.00 |
|             449 |            1.00 |        2,294.63 |     4,35,800.00 |
|             450 |            1.00 |        2,306.81 |     4,33,500.00 |
|             451 |            1.00 |        2,107.93 |     4,74,400.00 |
|             452 |            1.00 |        2,210.92 |     4,52,300.00 |
|             453 |            1.00 |        2,233.64 |     4,47,700.00 |
|             454 |            1.00 |        2,213.86 |     4,51,700.00 |
|             455 |            1.00 |        2,228.66 |     4,48,700.00 |
|             456 |            1.00 |        2,039.57 |     4,90,300.00 |
|             457 |            1.00 |        2,296.21 |     4,35,500.00 |
|             458 |            1.00 |        2,233.14 |     4,47,800.00 |
|             459 |            1.00 |        2,275.31 |     4,39,500.00 |
|             460 |            1.00 |        2,261.42 |     4,42,200.00 |
|             461 |            1.00 |        2,286.24 |     4,37,400.00 |
|             462 |            1.00 |        2,242.15 |     4,46,000.00 |
|             463 |            1.00 |        2,223.70 |     4,49,700.00 |
|             464 |            1.00 |        2,289.90 |     4,36,700.00 |
|             465 |            1.00 |        2,202.64 |     4,54,000.00 |
|             466 |            1.00 |        2,257.85 |     4,42,900.00 |
|             467 |            1.00 |        2,205.56 |     4,53,400.00 |
|             468 |            1.00 |        2,048.34 |     4,88,200.00 |
|             469 |            1.00 |        2,272.21 |     4,40,100.00 |
|             470 |            1.00 |        2,235.64 |     4,47,300.00 |
|             471 |            1.00 |        2,231.64 |     4,48,100.00 |
|             472 |            1.00 |        2,207.51 |     4,53,000.00 |
|             473 |            1.00 |        2,307.87 |     4,33,300.00 |
|             474 |            1.00 |        2,283.63 |     4,37,900.00 |
|             475 |            1.00 |        2,156.10 |     4,63,800.00 |
|             476 |            1.00 |        2,297.79 |     4,35,200.00 |
|             477 |            1.00 |        2,253.27 |     4,43,800.00 |
|             478 |            1.00 |        2,301.50 |     4,34,500.00 |
|             479 |            1.00 |        2,307.87 |     4,33,300.00 |
|             480 |            1.00 |        2,189.62 |     4,56,700.00 |
|             481 |            1.00 |        2,295.68 |     4,35,600.00 |
|             482 |            1.00 |        2,268.09 |     4,40,900.00 |
|             483 |            1.00 |        2,240.65 |     4,46,300.00 |
|             484 |            1.00 |        2,266.03 |     4,41,300.00 |
|             485 |            1.00 |        2,232.64 |     4,47,900.00 |
|             486 |            1.00 |        2,258.87 |     4,42,700.00 |
|             487 |            1.00 |        2,234.14 |     4,47,600.00 |
|             488 |            1.00 |        2,223.70 |     4,49,700.00 |
|             489 |            1.00 |        2,222.72 |     4,49,900.00 |
|             490 |            1.00 |        2,283.11 |     4,38,000.00 |
|             491 |            1.00 |        2,312.14 |     4,32,500.00 |
|             492 |            1.00 |        2,305.74 |     4,33,700.00 |
|             493 |            1.00 |        2,245.68 |     4,45,300.00 |
|             494 |            1.00 |        2,289.38 |     4,36,800.00 |
|             495 |            1.00 |        2,203.61 |     4,53,800.00 |
|             496 |            1.00 |        2,217.29 |     4,51,000.00 |
|             497 |            1.00 |        2,307.34 |     4,33,400.00 |
|             498 |            1.00 |        2,295.16 |     4,35,700.00 |
|             499 |            1.00 |        2,292.00 |     4,36,300.00 |
|             500 |            1.00 |        2,296.74 |     4,35,400.00 |


## Benchmark Assertions

* [PASS] Expected [Counter] TestCounter to must be greater than 1,000.00 operations; actual value was 2,165.12 operations.


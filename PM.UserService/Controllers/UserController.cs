﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PM.UserService.Model;
using PM.UserService.Repository;
using PM.UserService.ViewModel;

namespace PM.UserService.Controllers
{
    [Produces("application/json")]
   // [Route("user")]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        #region Constructor
        private readonly IUserRepository userRepository;

        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        #endregion

        // GET: api/User
        [HttpGet]
        public IActionResult Get()
        {
            var users = userRepository.GetAllUsers();
            return new OkObjectResult(users);
            //return new string[] { "value1", "value2" };
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(long userId)
        {
            var user = userRepository.GetUser(userId);
            return new OkObjectResult(user);
            //return "value";
        }

    
        [HttpPost("add")]
        public IActionResult  Add(User user)
        {
             userRepository.Add(user);
            return Ok(user);
           // return CreatedAtAction(nameof(Get), new { id = user.id }, user);
        }
        // POST: api/User
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            userRepository.Add(user);
            return Ok(user);
            
        }
        [HttpPost("update")]
        public IActionResult update(User usr)
        {
              userRepository.Update(usr);

            return new OkResult();
        }

        [HttpPost("delete/{userId}")]
        public IActionResult Delete(long userId)
        {
            var status=  userRepository.Delete(userId);

            return new OkResult();
        }

    }
}
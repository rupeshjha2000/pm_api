﻿using Microsoft.EntityFrameworkCore;
using PM.UserService.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;


namespace PM.UserService.DBContext
{
    [ExcludeFromCodeCoverage]
    public class PMContext : DbContext
    {
        public PMContext(DbContextOptions options)
           : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<User> Users { get; set; }
    }
}

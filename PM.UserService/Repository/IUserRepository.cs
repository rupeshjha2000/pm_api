﻿using PM.UserService.Model;
using PM.UserService.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PM.UserService.Repository
{
    public interface IUserRepository
    {
        User Add(User user);
        User Update(User user);
        bool Delete(long id);
        User GetUser(long id);
        IList<User> GetAllUsers();
        userViewModel GetUsers(searchUser search);

    }
}

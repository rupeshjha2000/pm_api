﻿using PM.UserService.DBContext;
using PM.UserService.Model;
using PM.UserService.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;


namespace PM.UserService.Repository
{
   
    public class UserRepository : IUserRepository
    {
        private readonly PMContext _pmContext;

        public UserRepository(PMContext pmContext)
        {
            this._pmContext = pmContext;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUser(long id)
        {
            var usr = _pmContext.Users.First(c => c.id == id);
            return usr;
           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<User> GetAllUsers()
        {
            var list = _pmContext.Users.ToList();

            return list;
          
        }

        public userViewModel GetUsers(searchUser search)
        {
            userViewModel model = new userViewModel();
            var propertyInfo = typeof(User).GetProperty(search.sortBy);

            model.users = _pmContext.Users.OrderBy(x => propertyInfo.GetValue(x, null)).Skip((search.pageNo - 1) * 10)
                .Where(c => (c.firstName.ToUpper() + " " + c.lastName.ToUpper() + " " + c.empId.ToUpper()).Contains(search.searchText.ToUpper())).Take(10).ToList();

            model.count = _pmContext.Users.Where(c => (c.firstName.ToUpper() + " " + c.lastName.ToUpper() + " " + c.empId.ToUpper()).Contains(search.searchText.ToUpper())).ToList().Count();


            return model;
          

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            var usr = _pmContext.Users.First(c => c.id == id);
            _pmContext.Users.Attach(usr);
            _pmContext.Entry(usr).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            var cnt =  _pmContext.SaveChanges();
            if (cnt > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public  User Add(User user)
        {
            if (_pmContext.Users.Any(c => c.empId == user.empId))
            {
                throw new Exception("Duplicate Employee");
            }
            _pmContext.Users.Add(user);
            _pmContext.SaveChanges();
            return user;
        }


        public  User Update(User user)
        {
            if (_pmContext.Users.Any(c => c.empId == user.empId && c.id != user.id))
            {
                throw new Exception("Duplicate Employee");
            }

            _pmContext.Users.Attach(user);
            _pmContext.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
             _pmContext.SaveChanges();
            return user;
        }

        
    }
}

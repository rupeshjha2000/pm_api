﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PM.UserService.Model
{
    [Table("tbl_user")]
    public class User
    {
        [Key]
        [Column("id")]
        public long id { get; set; }

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string empId { get; set; }
    }
}

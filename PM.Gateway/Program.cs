﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

namespace PM.Gateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }
        static readonly string AllowSpecificOrigins = "_AllowSpecificOrigins";
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
               .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("ocelot.json")
                    .AddEnvironmentVariables();
                })
             .ConfigureServices(s =>
             {
                 s.AddCors(options =>
                 {
                     options.AddPolicy(AllowSpecificOrigins,
                     builder =>
                     {
                         builder.AllowAnyOrigin()
                                 .AllowAnyHeader()
                                 .AllowAnyMethod();
                     });
                 });
                 s.AddOcelot();
             })
             .Configure(app =>
             {
                 app.UseCors(AllowSpecificOrigins);
                 app.UseOcelot().Wait();
             })
            .UseIISIntegration();
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PMService.Controllers;
using PMService.Model;
using PMService.Repository;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace PM.Test
{
    public  class ProjectTest
    {
        ProjectController _projectController;
        Mock<IProjectRepository> mockrepo = new Mock<IProjectRepository>();
        public ProjectTest()
        {
            this._projectController = new ProjectController(mockrepo.Object);
            this._projectController = new ProjectController(mockrepo.Object)
            {
                ControllerContext = new ControllerContext()
                { HttpContext = new DefaultHttpContext() }
            };
        }
        [Test]
        public void  GetAllProjects()
        {

            var projects = new List<Project>() {
                  new Project
                 {
                     id = 2,
                     projectName = "JHEA Finance",
                     managerName="Tanmoy Ghosh",
                     managerId=1,
                     dateCheck=true,
                     startDate=Convert.ToDateTime("2020-04-17 00:00:00.0000000"),
                     endDate=Convert.ToDateTime("2020-04-25 00:00:00.0000000"),
                     isActive=false,
                     isCompleted=true,
                     priority = 19
                 }
        };
            
            mockrepo.Setup(service => service.GetAllProjects()).Returns(projects);
            
            var response =(OkObjectResult) _projectController.Get();
            response.ShouldNotBeNull();
            var projList = (List<Project>)response.Value;
            projList.Count.ShouldBeGreaterThan(0);
            projList.Count.ShouldBeGreaterThanOrEqualTo(projects.Count);
            projList[0].projectName.ShouldBe(projects[0].projectName);
        }


        [Test]
        public void AdProject()
        {
            var project = new Project
            {
                id = 2,
                projectName = "JHEA Finance",
                managerName = "Tanmoy Ghosh",
                managerId = 1,
                dateCheck = true,
                startDate = Convert.ToDateTime("2020-04-17 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-25 00:00:00.0000000"),
                isActive = false,
                isCompleted = true,
                priority = 19
            };

            ProjectViewModel returnData = new ProjectViewModel() { Result = true, Message = "success", project = project };


            var request = new Project
            {
                id = 2,
                projectName = "JHEA Finance",
                managerName = "Tanmoy Ghosh",
                managerId = 1,
                dateCheck = true,
                startDate = Convert.ToDateTime("2020-04-17 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-25 00:00:00.0000000"),
                isActive = false,
                isCompleted = true,
                priority = 19
            };

            mockrepo.Setup(c => c.Add(It.IsAny<Project>())).Returns(returnData);

            //mockUserRepo.Setup(service => service.Add(user));


            OkObjectResult uvResult = (OkObjectResult)_projectController.Post(request);

            //Assert.AreEqual(200, uvResult.StatusCode);

            uvResult.ShouldNotBeNull();
            uvResult.StatusCode.Value.ShouldBe(200);

            var projList = (ProjectViewModel)uvResult.Value;
            projList.Result.ShouldBe(true);
            projList.project.projectName.ShouldBe(project.projectName);

        }


        [Test]
        public void ModifyProject()
        {

            var project = new Project
            {
                id = 2,
                projectName = "JHEA Finance",
                managerName = "Tanmoy Ghosh",
                managerId = 1,
                dateCheck = true,
                startDate = Convert.ToDateTime("2020-04-17 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-25 00:00:00.0000000"),
                isActive = false,
                isCompleted = true,
                priority = 19
            };

            ProjectViewModel returnData = new ProjectViewModel() { Result = true, Message = "success", project = project };


            var request = new Project
            {
                id = 2,
                projectName = "JHEA Finance",
                managerName = "Tanmoy Ghosh",
                managerId = 1,
                dateCheck = true,
                startDate = Convert.ToDateTime("2020-04-17 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-25 00:00:00.0000000"),
                isActive = false,
                isCompleted = true,
                priority = 19
            };
            int id =2;

            mockrepo.Setup(c => c.Update(It.IsAny<Project>())).Returns(returnData);
            var response = (JsonResult)_projectController.Put(id, request);
            response.ShouldNotBeNull();
            ProjectViewModel uvResult = (ProjectViewModel)response.Value;
            uvResult.ShouldBe(returnData);
            uvResult.Message.ShouldBe(returnData.Message);
        }

        [Test]
        public void Delete()
        {
            var project = new Project
            {
                id = 2,
                projectName = "JHEA Finance",
                managerName = "Tanmoy Ghosh",
                managerId = 1,
                dateCheck = true,
                startDate = Convert.ToDateTime("2020-04-17 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-25 00:00:00.0000000"),
                isActive = false,
                isCompleted = true,
                priority = 19
            };
            ProjectViewModel returnData = new ProjectViewModel() { Result = true, Message = "success", project = project };
            int id = 2;

            mockrepo.Setup(c => c.Delete(It.IsAny<long>())).Returns(returnData);
            var response =(JsonResult) _projectController.Delete(id);
            response.ShouldNotBeNull();
            ProjectViewModel uvResult = (ProjectViewModel)response.Value;
            uvResult.ShouldBe(returnData);
            uvResult.Message.ShouldBe(returnData.Message);
        }
    }
}

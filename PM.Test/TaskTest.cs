﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PMService.Business;
using PMService.Controllers;
using PMService.Model;
using PMService.Repository;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace PM.Test
{
    public class TaskTest
    {

        TaskController taskController;
        Mock<ITaskRepository> mockrepo = new Mock<ITaskRepository>();
        Mock<ITaskBL> mockTaskBLrepo = new Mock<ITaskBL>();
        public TaskTest()
        {
            this.taskController = new TaskController(mockrepo.Object, mockTaskBLrepo.Object);
            this.taskController = new TaskController(mockrepo.Object, mockTaskBLrepo.Object)
            {
                ControllerContext = new ControllerContext()
                { HttpContext = new DefaultHttpContext() }
            };
        }

        [Test]
        public void GetAllTask()
        {

            var projectTask = new List<TaskDataModel>() {
                  new TaskDataModel
                 {
                     id = 1,
                     taskName = "Treaty calculator",
                     parentTask=true,
                     parentTaskId=0,
                     projectId=2,
                     parentTaskName="",
                     projectName="JHEA Finance",
                     userId=1,
                     userName="Rupesh Jha",
                     startDate=Convert.ToDateTime("2020-04-18 00:00:00.0000000"),
                     endDate=Convert.ToDateTime("2020-04-19 00:00:00.0000000"),
                     isCompleted=false,
                     priority = 18
                 }
        };

            mockTaskBLrepo.Setup(service => service.GetAll()).Returns(projectTask);

            var response = (OkObjectResult)taskController.Get();
            response.ShouldNotBeNull();
            response.StatusCode.Value.ShouldBe(200);
            var taskList = (List<TaskDataModel>)response.Value;
            taskList.Count.ShouldBeGreaterThan(0);
            taskList.Count.ShouldBeGreaterThanOrEqualTo(projectTask.Count);
            taskList.ShouldBe(projectTask);
            taskList[0].projectName.ShouldBe(projectTask[0].projectName);

        }

        [Test]
        public void AddTask()
        {
            var request = new ProjectTask
            {
                id = 1,
                taskName = "Treaty calculator",
                parentTask = true,
                parentTaskId = 0,
                projectId = 2,

                userId = 1,

                startDate = Convert.ToDateTime("2020-04-18 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-19 00:00:00.0000000"),
                isCompleted = false,
                priority = 18
            };

            mockTaskBLrepo.Setup(service => service.Add(It.IsAny<ProjectTask>())).Returns(request);
            var response = (OkObjectResult)taskController.Post(request);
            response.ShouldNotBeNull();
            response.StatusCode.Value.ShouldBe(200);
            var projTask = (ProjectTask)response.Value;
            projTask.taskName.ShouldBe(request.taskName);
            projTask.ShouldBe(request);
        }

        [Test]
        public void ModifyTask()
        {
            var projectTask = new List<TaskDataModel>() {
                  new TaskDataModel
                 {
                     id = 1,
                     taskName = "Treaty calculator",
                     parentTask=true,
                     parentTaskId=0,
                     projectId=2,
                     parentTaskName="",
                     projectName="JHEA Finance",
                     userId=1,
                     userName="Rupesh Jha",
                     startDate=Convert.ToDateTime("2020-04-18 00:00:00.0000000"),
                     endDate=Convert.ToDateTime("2020-04-19 00:00:00.0000000"),
                     isCompleted=false,
                     priority = 18
                 }
            };
            TaskViewaModel tvm = new TaskViewaModel() { Result = true, Message = "success", ProjectTask = projectTask };
            var request = new ProjectTask
            {
                id = 1,
                taskName = "Treaty calculator",
                parentTask = true,
                parentTaskId = 0,
                projectId = 2,

                userId = 1,

                startDate = Convert.ToDateTime("2020-04-18 00:00:00.0000000"),
                endDate = Convert.ToDateTime("2020-04-19 00:00:00.0000000"),
                isCompleted = false,
                priority = 18
            };

            mockTaskBLrepo.Setup(c => c.Update(It.IsAny<ProjectTask>())).Returns(tvm);
            var response = (JsonResult)taskController.Put(1, request);
            response.ShouldNotBeNull();
            var responseVal = (TaskViewaModel)response.Value;
            responseVal.Result.ShouldBe(true);
            responseVal.ShouldBe(tvm);
        }
    }
}

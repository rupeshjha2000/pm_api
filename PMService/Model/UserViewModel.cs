﻿using PM.UserService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Model
{
    public class UserViewModel
    {
        public User user { get; set; }
        public string Message { get; set; }
        public bool Result { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Model
{
    [Table("tbl_project")]
    public class Project
    {
        [Key]
        [Column("id")]
        public long id { get; set; }
        public string projectName { get; set; }
        public string managerName { get; set; }
        public long? managerId { get; set; }
        public int priority { get; set; }
        public bool dateCheck { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public Boolean isCompleted { get; set; }
        public Boolean isActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Model
{
    public class TaskViewaModel
    {
        public IList<TaskDataModel> ProjectTask { get; set; }
        public string Message { get; set; }
        public bool Result { get; set; }
    }

    public class TaskDataModel
    {
        public long id { get; set; }
        public string taskName { get; set; }
        public int projectId { get; set; }
        public string projectName { get; set; }
        public bool parentTask { get; set; }
        public int parentTaskId { get; set; }
        public string parentTaskName { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public int userId { get; set; }
        public string userName { get; set; }
        public bool isCompleted { get; set; }
        public int priority { get; set; }
    }
}

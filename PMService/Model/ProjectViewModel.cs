﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Model
{
    public class ProjectViewModel
    {
        public Project project { get; set; }
        public string Message { get; set; }
        public bool Result { get; set; }
    }
}

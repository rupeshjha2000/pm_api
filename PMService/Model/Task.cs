﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Model
{
    [Table("tbl_task")]
    public class ProjectTask
    {
        [Key]
        [Column("taskId")]
        public long id { get; set; }
        public string taskName { get; set; }
        public bool parentTask { get; set; }
        public int parentTaskId { get; set; }
        public int projectId { get; set; }
      
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }

        public int userId { get; set; }
      
        public bool isCompleted { get; set; }
        public int priority { get; set; }
    }
}

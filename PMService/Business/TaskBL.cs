﻿using PMService.Model;
using PMService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Business
{
    public interface ITaskBL
    {
        IList<TaskDataModel> GetAll();
        IList<TaskDataModel> GetProjectTasks(long projectId);
        ProjectTask Add(ProjectTask project);
        TaskViewaModel Update(ProjectTask project);
        TaskViewaModel UpdateTaskToComplete(int id, ProjectTask task);

    }

    public class TaskBL: ITaskBL
    {
        private ITaskRepository _taskrepo;
        public TaskBL(ITaskRepository taskrepo)
        {
            this._taskrepo = taskrepo;
        }

        public IList<TaskDataModel> GetAll()
        {
            var list = _taskrepo.GetAll();

            return list;
        }
        public IList<TaskDataModel> GetProjectTasks(long projectId)
        {
            var list = _taskrepo.GetProjectTasks(projectId);

            return list;
        }
        public ProjectTask Add(ProjectTask project)
        {
          return  _taskrepo.Add(project);
            
        }

        public TaskViewaModel Update(ProjectTask projTask)
        {
            return  _taskrepo.Update(projTask);
        }
        public TaskViewaModel UpdateTaskToComplete(int id, ProjectTask task)
        {
            return _taskrepo.UpdateTaskToComplete(id,task);
        }
    }
}

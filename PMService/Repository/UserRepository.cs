﻿using PM.UserService.DBContext;
using PM.UserService.Model;
using PMService.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;


namespace PM.UserService.Repository
{
   
    public class UserRepository : IUserRepository
    {
        private readonly PMContext _pmContext;

        public UserRepository(PMContext pmContext)
        {
            this._pmContext = pmContext;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUser(long id)
        {
            var usr = _pmContext.Users.First(c => c.id == id);
            return usr;
           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<User> GetAllUsers()
        {
            var list = _pmContext.Users.ToList();

            return list;
          
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserViewModel Add(User user)
        {
            var result = true;
            var message = "success";
            if (_pmContext.Users.Any(c => c.empId == user.empId))
            {
                result = false;
                message = "Employee Id Already Exists";
            }

            try
            {
                _pmContext.Users.Add(user);
                _pmContext.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return new UserViewModel() { Result = result, Message = message, user = user };

        }


        public UserViewModel Update(User userdata)
        {
            var result = true;
            var message = "success";
            if (_pmContext.Users.Any(c => c.empId == userdata.empId && c.id != userdata.id))
            {
                result = false;
                message = "Employee Id Exists with som other user";
                // return new UserViewModel() { Result = false, Message = "Employee Id Exists with som other user", user = userdata };
            }

            try
            {
                _pmContext.Users.Attach(userdata);
                _pmContext.Entry(userdata).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _pmContext.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return new UserViewModel() { Result = result, Message = message, user = userdata }; ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserViewModel Delete(long id)
        {
            var result = true;
            var message = "success";
            User usr = new User();
            try
            {
             usr = _pmContext.Users.First(c => c.id == id);
            _pmContext.Users.Attach(usr);
            _pmContext.Entry(usr).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            var cnt =  _pmContext.SaveChanges();

                if (cnt == 0)
                {
                    result = false;
                    message = "failed";
                }
               
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }
            return new UserViewModel() { Result = result, Message = message, user = usr }; ;
        }

       

        
    }
}

﻿using PM.UserService.Model;
using PMService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PM.UserService.Repository
{
    public interface IUserRepository
    {
        UserViewModel Add(User user);
        UserViewModel Update(User user);
        UserViewModel Delete(long id);
        User GetUser(long id);
        IList<User> GetAllUsers();

    }
}

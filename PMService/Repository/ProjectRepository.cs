﻿using PM.UserService.DBContext;
using PMService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Repository
{
    public class ProjectRepository: IProjectRepository
    {
        private readonly PMContext _pmContext;

        public ProjectRepository(PMContext pmMContext)
        {
            this._pmContext = pmMContext;
        }
        public IList<Project> GetAllProjects()
        {
            var list = _pmContext.Projects.ToList();

            return list;
        }
        public ProjectViewModel Add(Project project)
        {
            var result = true;
            var message = "success";
            if (_pmContext.Projects.Any(c => c.projectName == project.projectName))
            {
                result = false;
                message = "Duplicate Project Name";
                return new ProjectViewModel() { Result = result, Message = message, project = project };
            }

            try
            {
                _pmContext.Projects.Add(project);
                _pmContext.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return new ProjectViewModel() { Result = result, Message = message, project = project };

            
        }
        public ProjectViewModel Update(Project project)
        {
           
            var result = true;
            var message = "success";
            if (_pmContext.Projects.Any(c => c.projectName == project.projectName && c.id != project.id))
            {
                result = false;
                message = "Duplicate Project Name";
                return new ProjectViewModel() { Result = result, Message = message, project = project };
            }

            try
            {
                _pmContext.Projects.Attach(project);
                _pmContext.Entry(project).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _pmContext.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return new ProjectViewModel() { Result = result, Message = message, project = project }; ;
        }


        public ProjectViewModel Delete(long id)
        {
            
           
            var result = true;
            var message = "success";
            Project prj = new Project();
            try
            {
                 prj = _pmContext.Projects.First(c => c.id == id);
                _pmContext.Projects.Attach(prj);
                _pmContext.Entry(prj).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                var cnt = _pmContext.SaveChanges();

                if (cnt == 0)
                {
                    result = false;
                    message = "failed";
                }

            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }
            return new ProjectViewModel() { Result = result, Message = message, project = prj }; ;
        }

     
        //public Task<ProjectViewModel> GetProjects(searchProject search)
        //{
        //    ProjectViewModel model = new ProjectViewModel();
        //    var propertyInfo = typeof(Project).GetProperty(search.sortBy);

        //    model.Projects = _pmContext.Projects.OrderBy(x => propertyInfo.GetValue(x, null)).Skip((search.pageNo - 1) * 10)
        //        .Where(c => c.ProjectName.ToUpper().Contains(search.searchText.ToUpper())).Take(10).ToList();

        //    model.count = _pmContext.Projects.Where(c => c.ProjectName.ToUpper().Contains(search.searchText.ToUpper())).ToList().Count();

        //    return Task.FromResult<ProjectViewModel>(
        //      model
        //    );
        //}

        public int overdueProj()
        {
            return _pmContext.Projects.Where(c => c.endDate < DateTime.Now && c.isCompleted == false).ToList().Count();
        }

        public int ProjectCount(bool IsCompleted)
        {
            return _pmContext.Projects.Where(c => c.isCompleted == IsCompleted).ToList().Count();
        }

       

        public bool UserExists(long userId)
        {
            var exists = _pmContext.Projects.Any(c => c.managerId == userId);
            return exists;
          
        }
    }
}

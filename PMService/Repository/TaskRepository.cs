﻿using PM.UserService.DBContext;
using PMService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Repository
{
    public class TaskRepository: ITaskRepository
    {
        private readonly PMContext _pmContext;

        public TaskRepository(PMContext pmContext)
        {
            this._pmContext = pmContext;
        }
        public IList<TaskDataModel> GetAll()
        {
            //var list = _pmContext.Tasks.ToList();

            var list = (from t in _pmContext.Tasks
                         join p in _pmContext.Projects on t.projectId equals p.id
                         join u in _pmContext.Users    on t.userId equals u.id
                         join pt in _pmContext.Tasks   on t.parentTaskId equals pt.id into gj
                        from x in gj.DefaultIfEmpty()
                        select new TaskDataModel
                         {
                            id=t.id,
                            taskName=t.taskName,
                            userId=t.userId,
                            userName=u.firstName+" "+u.lastName,
                            projectId=t.projectId,
                            projectName=p.projectName,
                            endDate=t.endDate,
                            startDate = t.startDate,
                            isCompleted =t.isCompleted,
                            parentTask=t.parentTask,
                            parentTaskId=t.parentTaskId,
                            priority =t.priority,
                            parentTaskName = (x == null ? String.Empty : x.taskName)
                        }).ToList();

            return list;
        }

        public ProjectTask Add(ProjectTask project)
        {
            _pmContext.Tasks.Add(project);
             _pmContext.SaveChanges();
            return project;
        }

        public TaskViewaModel Update(ProjectTask projTask)
        {
            var result = true;
            var message = "success";
            IList<TaskDataModel> allTAskData = new List<TaskDataModel>();
            var exists = _pmContext.Tasks.Any(c => c.id == projTask.id && c.isCompleted == true);

            if (exists)
            {
                result = false;
                message = "Task is already completed. You cannot make change to completed task";

                return new TaskViewaModel() { Result = result, Message = message, ProjectTask = allTAskData };
            }

            _pmContext.Tasks.Attach(projTask);
            _pmContext.Entry(projTask).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _pmContext.SaveChanges();

            return new TaskViewaModel() { Result = result, Message = message, ProjectTask = allTAskData }; ;
        }

        public TaskViewaModel UpdateTaskToComplete(int id, ProjectTask task)
        {
            var result = true;
            var message = "success";
            IList<TaskDataModel> allTAskData = new List<TaskDataModel>();
            var exists = _pmContext.Tasks.Any(c => c.parentTaskId == task.id && c.isCompleted == false);

            if (exists)
            {
                result = false;
                message = "Please complete all child task of the selected task before completing this";
                
                return new TaskViewaModel() { Result = result, Message = message, ProjectTask = allTAskData };
            }
            task.isCompleted = true;
            var updateResult=Update(task);
            if(!updateResult.Result)
                return updateResult;

            allTAskData = GetAll();
            return new TaskViewaModel() { Result = result, Message = message, ProjectTask = allTAskData };
        }
        public bool Delete(long id)
        {
            var task = _pmContext.Tasks.First(c => c.id == id);
            _pmContext.Tasks.Attach(task);
            _pmContext.Entry(task).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            var cnt =  _pmContext.SaveChanges();
            if (cnt > 0)
                return true;
            else
                return false;
        }

        public IList<TaskDataModel> GetProjectTasks( long projectId)
        {
            var list = (from t in _pmContext.Tasks
                        join p in _pmContext.Projects on t.projectId equals p.id
                        join u in _pmContext.Users on t.userId equals u.id
                        join pt in _pmContext.Tasks on t.parentTaskId equals pt.id into gj
                        from x in gj.DefaultIfEmpty()
                        where t.projectId==projectId
                        select new TaskDataModel
                        {
                            id = t.id,
                            taskName = t.taskName,
                            userId = t.userId,
                            userName = u.firstName + " " + u.lastName,
                            projectId = t.projectId,
                            projectName = p.projectName,
                            endDate = t.endDate,
                            startDate = t.startDate,
                            isCompleted = t.isCompleted,
                            parentTask = t.parentTask,
                            parentTaskId = t.parentTaskId,
                            priority = t.priority,
                            parentTaskName = (x == null ? String.Empty : x.taskName)
                        }).ToList();

            return list;
        }

       
    }
}

﻿using PMService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Repository
{
    public interface IProjectRepository
    {
        IList<Project> GetAllProjects();

        ProjectViewModel Add(Project project);
        ProjectViewModel Update(Project project);
        ProjectViewModel Delete(long id);
           
        bool UserExists(long userId);

        int ProjectCount(bool isCompleted);
        int overdueProj();
    }
   
}

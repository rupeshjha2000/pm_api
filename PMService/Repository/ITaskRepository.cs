﻿using PMService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMService.Repository
{
    public interface ITaskRepository
    {
        IList<TaskDataModel> GetAll();

        ProjectTask Add(ProjectTask project);
        TaskViewaModel Update(ProjectTask project);
        bool Delete(long id);

        TaskViewaModel UpdateTaskToComplete(int id, ProjectTask task);

        IList<TaskDataModel> GetProjectTasks(long projectId);
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMService.Model;
using PMService.Repository;

namespace PMService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectRepository _projRepository;
        public ProjectController(IProjectRepository projRepository)
        {
            this._projRepository = projRepository;
        }

        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            var projects = _projRepository.GetAllProjects();
            return new OkObjectResult(projects);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Project project)
        {

            var projectViewModel = _projRepository.Add(project);
            return Ok(projectViewModel);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Project project)
        {
            
            var projectViewModel = _projRepository.Update(project);

            return new JsonResult(projectViewModel);
        }

        // DELETE api/values/5
        [HttpPost("delete/{id}")]
        public IActionResult Delete(int id)
        {
            var projectViewModel = _projRepository.Delete(id);

            return new JsonResult(projectViewModel);
        }
    }
}

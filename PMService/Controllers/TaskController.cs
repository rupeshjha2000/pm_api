﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMService.Business;
using PMService.Model;
using PMService.Repository;

namespace PMService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskRepository _taskRepository;
        private readonly ITaskBL _taskBL;
        public TaskController(ITaskRepository taskRepository, ITaskBL taskBL)
        {
            this._taskRepository = taskRepository;
            this._taskBL = taskBL;
        }

        // GET: api/Task
       
        [HttpGet]
        public IActionResult Get()
        {
            var tasks = _taskBL.GetAll();
            return new OkObjectResult(tasks);
        }
        [HttpPost("getprojectTasks/{projectId}")]
        public IActionResult GetprojectTasks( long projectId)
        {
            var task=  _taskBL.GetProjectTasks(projectId);
            return new OkObjectResult(task);
        }
        // GET: api/Task/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Task
        [HttpPost]
        public IActionResult Post([FromBody] ProjectTask task)
        {
            var taskViewModel = _taskBL.Add(task);
            return Ok(taskViewModel);
        }

        // PUT: api/Task/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProjectTask task)
        {
            var taskViewModel = _taskBL.Update(task);
            return new JsonResult(taskViewModel);
        }

        [HttpPost("updateTaskToComplete/{id}")]
        public IActionResult UpdateTaskToComplete(int id, [FromBody] ProjectTask task)
        {
            var taskViewModel = _taskBL.UpdateTaskToComplete(id,task);
            return new JsonResult(taskViewModel);
        }
        // DELETE: api/ApiWithActions/5

        [HttpPost("delete/{id}")]
        public IActionResult Delete(int id)
        {
            var taskViewModel = _taskRepository.Delete(id);
            return new JsonResult(taskViewModel);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PM.UserService.Model;
using PM.UserService.Repository;

namespace PMService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        // GET: api/User
        [HttpGet]
        public IActionResult Get()
        {
            var users = userRepository.GetAllUsers();
            return new OkObjectResult(users);
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var user = userRepository.GetUser(id);
            return new OkObjectResult(user);
        }

        // POST: api/User
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            var userViewModel=userRepository.Add(user);
            return Ok(userViewModel);
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]  User user)
        {
           var userViewModel= userRepository.Update(user);

           return new JsonResult(userViewModel);
            //return new OkResult();
        }

        [HttpPost("delete/{userId}")]
        public IActionResult Delete(long userId)
        {
            var userViewModel = userRepository.Delete(userId);

           return  new JsonResult(userViewModel);
        }
    }
}

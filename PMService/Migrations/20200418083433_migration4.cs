﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PM.UserService.Migrations
{
    public partial class migration4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "taskOwnerId",
                table: "tbl_task",
                newName: "userId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "userId",
                table: "tbl_task",
                newName: "taskOwnerId");
        }
    }
}

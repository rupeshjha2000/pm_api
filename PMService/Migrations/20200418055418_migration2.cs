﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PM.UserService.Migrations
{
    public partial class migration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "managerId",
                table: "tbl_project",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<bool>(
                name: "isActive",
                table: "tbl_project",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isActive",
                table: "tbl_project");

            migrationBuilder.AlterColumn<long>(
                name: "managerId",
                table: "tbl_project",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}

﻿using PM.UserService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PM.UserService.ViewModel
{
    public class userViewModel
    {
        public List<User> users { get; set; }
        public int count { get; set; }
    }

    public class searchUser {
        public int pageNo { get; set; }
        public string sortBy { get; set; }
        public string searchText { get; set; }
        public string isAsc { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using PM.UserService.Model;
using PMService.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;


namespace PM.UserService.DBContext
{
    [ExcludeFromCodeCoverage]
    public class PMContext : DbContext
    {
        public PMContext(DbContextOptions options)
           : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> Tasks { get; set; }
    }
}
